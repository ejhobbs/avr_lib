/** @file timer.h
 * @brief Functions for getting time since boot
 * @author Edward Hobbs (ejhobbs)
 */
#ifndef TIMER_LIB
#define TIMER_LIB

/**
 * Durations of time we can query
 */
typedef enum period {
  MILLIS,
  SECS,
  MINS
} period;

/**
 * Return length of period p, since boot
 * @param p time period to query
*/
unsigned long timer_time(period p);

/**
 * Initialise internal timers
 */
void timer_init();
#endif
