/**
 * This code is mostly borrowed from here:
 * https://gist.github.com/adnbr/2439125
 * Used to count the number of milliseconds that have passed since boot
*/
#include <stddef.h>
#include <timer.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#define CTC_MATCH_OVERFLOW ((F_CPU / 1000) /8)

volatile unsigned long millis = 0;
volatile unsigned long secs = 0;
volatile unsigned long mins = 0;
ISR(TIMER1_COMPA_vect){
  // will fire once every millisecond
  millis++;
  secs = millis/1000;
  mins = secs/60;
}

unsigned long timer_time(period p) {
  switch (p) {
    case MILLIS:
      return (unsigned long) millis;
    case SECS:
      return (unsigned long) secs;
    case MINS:
      return (unsigned long) mins;
    default:
      return 0; //unrecognised period
  }
}

void timer_init() {
  TCCR1B |= (1 << WGM12) | (1 << CS11);

  OCR1AH = (CTC_MATCH_OVERFLOW >> 8);
  OCR1AL = (CTC_MATCH_OVERFLOW);

  TIMSK1 |= (1 << OCIE1A);

  sei();
}
