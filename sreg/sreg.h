/** @file sreg.h
 * @brief Functions for interfacing with shift register
 * @author Edward Hobbs (ejhobbs)
 */

#ifndef SREG_LIB
#define SREG_LIB
/**
 * Struct for holding details about shift register
 */
typedef volatile struct sreg {
  volatile char *port; /**< AVR Port register is attached (e.g. PORTA) */
  char in; /**< Input pin for register */
  char out_en; /**< Output enable pin for register */
  char clk; /** < Clock pin for register */
  char reset; /** < Clear pin on register */
} sreg, *Psreg;

/**
 * Initialise shift register
 * Enables output, and writes 0x00
 * @param s Register to initialise
 */
void sreg_init(Psreg s);

/**
 * Push 8 bits to shift register. Will disable output, then re-enable afterward.
 * @param c Value to display
 * @param s Register to write to
 */
void sreg_push(char c, Psreg s);

/**
 * Clear all output on shift register
 */
void sreg_clear(Psreg s);

/**
 * Pulse clock signal on register
 * @param s Register
 */
void sreg_adv_clk(Psreg s);
#endif
