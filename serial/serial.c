#include<serial.h>
#include<avr/io.h>
#include<util/setbaud.h>

void uart_init(void) {
  /* Set Speed */
  UBRR0H = UBRRH_VALUE;
  UBRR0L = UBRRL_VALUE;

/* If double speed has been enabled */
#if USE_2X
  UCSR0A |= _BV(U2X0);
#else
  UCSR0A &= ~(_BV(U2X0));
#endif

  UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
  UCSR0B = _BV(RXEN0) | _BV(TXEN0); /* enable rx & tx */
}

char uart_can_get(void) {
  return UCSR0A & (1 << RXC0);
}

char uart_getchar(void) {
  return UDR0;
}

char uart_can_put(void) {
  return UCSR0A & (1 << UDRE0);
}

void uart_putchar(char c) {
  UDR0 = c;
}

char uart_bgetchar(void) {
  loop_until_bit_is_set(UCSR0A, RXC0);
  return UDR0;
}

void uart_bputchar(char c) {
  loop_until_bit_is_set(UCSR0A, UDRE0);
  UDR0 = c;
}

