/** @file serial.h
 * @brief Functions for interfacing with AVR UART
 * @author Edward Hobbs (ejhobbs)
 */

#ifndef SERIAL_LIB
#define SERIAL_LIB
#include <stdio.h>
/**
 * Initialise UART ready for transmission
 * Enables Rx & Tx, 8 bit transmission
 * Optional: if USE_2X defined, uses DDR
 */
void uart_init(void);

/**
 * Waits until uart is ready, then places char into reg
 * If \n sent, will convert to \r
 * @param c Character to send
 * @param stream Used for stdout/in redirection
 */
char uart_bgetchar(void);

/**
 * Waits for data to be transmitted, then reads
 * @returns received data from UART
 */
void uart_bputchar(char c);

/**
 * Checks if serial has bit to receive
 */
char uart_can_get(void);

char uart_getchar(void);

char uart_can_put(void);

void uart_putchar(char c);

#endif

#ifndef BAUD
#define BAUD 9600
#endif
