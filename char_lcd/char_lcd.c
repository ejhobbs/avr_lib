#include <char_lcd.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>

void lcd_init(Pdisplay d, char mode, char entry, char display) {
  lcd_write_cmd(d, LCD_FUNCTION_MODE | mode);
  lcd_write_cmd(d, LCD_ENTRY_MODE | entry);
  lcd_write_cmd(d, LCD_DISPLAY_MODE | display);
  lcd_write_cmd(d, LCD_CLEAR_DISP); //clear display and reset cursor position
}

void lcd_save_char(Pdisplay d, int loc, char * c) {
  lcd_write_cmd(d, LCD_CGRAM_ADDR + (8 * loc)); //set addr
  for (int i = 0; i < 8; i++) {
    lcd_write_data(d, c[i]);
  }
}

void lcd_write_left(Pdisplay d, int line, char* word) {
  lcd_write_offset(d, line, 0, word);
}

void lcd_write_right(Pdisplay d, int line, char* word) {
  int offset = LCD_NUM_COLS - strlen(word);
  lcd_write_offset(d, line, offset, word);
}

void lcd_write_center(Pdisplay d, int line, char* word) {
  int offset = (LCD_NUM_COLS - strlen(word)) / 2;
  lcd_write_offset(d, line, offset, word);
}

void lcd_write_offset(Pdisplay d, int line, int offset, char* word) {
  lcd_write_string(d, line+offset, word);
}

void lcd_write_string(Pdisplay d, int pos, char* word) {
  lcd_write_cmd(d, pos);
  for (int i = 0; i < strlen(word); i++) {
    lcd_write_data(d, word[i]);
  }
}

void lcd_write_data(Pdisplay d, char cmd) {
  *(d->port) |= d->rs; //rs high to input data
  *(d->port) &= ~(d->rw); //rw low to write
  sreg_push(cmd, d->data); //push data to pins ready to go
  lcd_pulse_clk(d);
  sreg_clear(d->data);
}

void lcd_write_cmd(Pdisplay d, char cmd) {
  *(d->port) &= ~(d->rs); //rs low to input instruction
  *(d->port) &= ~(d->rw); //rw low to write
  sreg_push(cmd, d->data); //push data to pins ready to go
  lcd_pulse_clk(d);
  sreg_clear(d->data);
  if(cmd == LCD_CURSOR_HOME || cmd == LCD_CLEAR_DISP) {
    //these instructions are quite slow in comparison
    _delay_ms(2);
  } else {
    _delay_us(50);
  }
}

static void lcd_pulse_clk(Pdisplay d) {
  *(d->port) |= d->en;
  _delay_us(10);
  *(d->port) &= ~(d->en);
  _delay_ms(10);
}
