/** @file char_lcd.h
 * @brief Functions for interfacing with character LCD
 *
 * @author Edward Hobbs (ejhobbs)
 *
 */
#ifndef SREG_LIB
#include "../sreg/sreg.h"
#endif

#ifndef CHAR_LCD_LIB
#define CHAR_LCD_LIB
/**
 * Structure to represent a character display
 */
typedef volatile struct display {
  volatile char *port; /**< AVR Port that display is connected to (e.g. PORTA) */
  Psreg data; /**< Shift registry connected to display data lines */
  int rs;/**< Pin connected to display "register select" */
  int rw;/**< Pin connected to display "read/write" */
  int en;/**< Pin connected to display "enable" */
} display, *Pdisplay;

/**
 * Save custom character to display memory
 * @param d display struct
 * @param loc Character location (0...7)
 * @param c Character, as 5x8 array
 */
void lcd_save_char(Pdisplay d, int loc, char* c);

/**
 * Write string on display, with offset on left side.
 * Useful for writing RTL without flipping display config
 * @param d display
 * @param line line to write on (0...CHAR_LCD_NUM_COLS-1)
 * @param word String to write
 */
void lcd_write_left(Pdisplay d, int line, char* word);

/**
 * Write string centered on display
 * @param d display
 * @param line line to write on
 * @param word String to write
 */
void lcd_write_center(Pdisplay d, int line, char* word);

/**
 * Write string offset from left by n
 * @param d display
 * @param line line to write on
 * @param offset how far along to start writing
 * @param word String to write
 */
void lcd_write_offset(Pdisplay d, int line, int offset, char* word);

/**
 * Write string to display
 * @param d display
 * @param line
 * @param word String to write
 */
void lcd_write_string(Pdisplay d, int line, char* word);

/**
 * Send data command to display
 * @param d display
 * @param cmd command
 */
void lcd_write_data(Pdisplay d, char cmd);

/**
 * Send instruction command to display
 * @param d display
 * @param cmd command
 */
void lcd_write_cmd(Pdisplay d, char cmd);

/**
 * Initialise display, clear, and reset cursor to home
 * @param d display struct
 * @param mode Function mode (ie 8bit/4bit)
 * @param entry Entry mode (e.g. cursor shift left/right, display shift)
 * @param display Display mode (cursor on/off, cursor blink, display on/off)
 */
void lcd_init(Pdisplay d, char mode, char entry, char display);

/**
 * Pulse enable pin on LCD
 * @param d display
 */
static void lcd_pulse_clk(Pdisplay d);
#endif

#ifndef CHAR_LCD_LIB_CMDS
#define CHAR_LCD_LIB_CMDS

#define LCD_CLEAR_DISP 0x01
#define LCD_CURSOR_HOME 0x02
#define LCD_ENTRY_MODE 0x04
#define LCD_DISPLAY_MODE 0x08
#define LCD_SHIFT_MODE 0x10
#define LCD_FUNCTION_MODE 0x20
#define LCD_CGRAM_ADDR 0x40
#define LCD_DDRAM_ADDR 0x80

#endif

#ifndef CHAR_LCD_LIB_ADDRESSES
#define CHAR_LCD_LIB_ADDRESSES

#define LCD_FIRST_CHAR 0x80
#define LCD_SECOND_CHAR 0xC0

#endif

#ifndef CHAR_LCD_LIB_DIMENSIONS
#define CHAR_LCD_LIB_DIMENSIONS

#define LCD_NUM_COLS 16
#define LCD_NUM_ROWS 2

#endif
